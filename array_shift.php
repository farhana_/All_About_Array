<?php

	$color = array(
		"a" => "red",
		"b" => "green",
		"c" => "blue",
		"d" => "yellow"
	);
	echo "<pre>";
	print_r($color);
	echo "<pre/>";
	
	//remove key and value from list
	array_shift($color);
	echo "<pre>";
	print_r($color);
	echo "<pre/>";
?>